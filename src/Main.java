import AerialVehicles.*;
import Entities.Coordinates;
import Entities.EnumsCreator.Camera;
import Entities.EnumsCreator.Sensor;
import Entities.EnumsCreator.Missle;
import Missions.AerialVehicleNotCompatibleException;
import Missions.AttackMission;
import Missions.BdaMission;
import Missions.IntelligenceMission;

public class Main {

    public static void main(String[] args) throws AerialVehicleNotCompatibleException {
        Coordinates TargetCords= new Coordinates(33.2033427805222,44.5176910795946); //only added the coordinates once because
        Coordinates BaseCords= new Coordinates(31.827604665263365,34.81739714569337);//they are the same for all scenarios
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////// first mission
        Shoval InformationJet = new Shoval(BaseCords);
        InformationMissionVehicle MyVehicle1 = new InformationMissionVehicle(Sensor.InfraRed,InformationJet);
        IntelligenceMission GetInformation1 = new IntelligenceMission("Tuwaitha Nuclear Research Center",TargetCords,"Dror zalicha",MyVehicle1);
        InformationJet.flyTo(TargetCords);
        InformationJet.HoverOverLocation(BaseCords);
        GetInformation1.begin();
        GetInformation1.finish();
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////// second mission
        F16 TheFighterJet = new F16(BaseCords);
        AttackMissionVehicle MyVehicle2 = new AttackMissionVehicle(250, Missle.AMRAM,TheFighterJet);
        AttackMission goAttack2 = new AttackMission("Tuwaitha Nuclear Research Center",TargetCords,"Ze'ev Raz",MyVehicle2);
        goAttack2.begin();
        goAttack2.finish();
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////// third mission
        F16 BdaFighterJet = new F16(BaseCords);
        BdaMissionVehicle MyVehicle3 = new BdaMissionVehicle(Camera.Thermal,BdaFighterJet);
        BdaMission goBda1 = new BdaMission("Tuwaitha Nuclear Research Center",TargetCords,"Ilan Ramon",MyVehicle3);
        goBda1.begin();
        goBda1.finish();
    }
}
