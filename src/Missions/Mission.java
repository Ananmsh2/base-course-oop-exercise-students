package Missions;

import AerialVehicles.AerialVehicle;
import Entities.Coordinates;

public abstract class Mission implements MissionAction{
    Coordinates TargetCords;
    String pilotName;


    AerialVehicle planeBeingUsed;

    public void begin()
    {
        System.out.println("beginning Mission!");
        this.planeBeingUsed.flyTo(this.TargetCords);
    }
    public void cancel()
    {
        System.out.println("Abort Mission!");
        this.planeBeingUsed.land(planeBeingUsed.baseCords);
    }
    public void finish() throws AerialVehicleNotCompatibleException {
        executeMission();
        this.planeBeingUsed.land(planeBeingUsed.baseCords);
        System.out.println("Finished Mission!");
    }


}
