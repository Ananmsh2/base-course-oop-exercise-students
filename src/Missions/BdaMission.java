package Missions;

import AerialVehicles.BdaMissionVehicle;
import Entities.Coordinates;

public class BdaMission extends Mission{

    String Objective=" ";
    public BdaMission(String Objective, Coordinates targetCords, String pilotname, BdaMissionVehicle planetouse) {
        this.Objective = Objective;
        super.pilotName=pilotname;
        super.planeBeingUsed=planetouse;
        super.TargetCords =targetCords;
    }
    @Override
    public void executeMission() throws AerialVehicleNotCompatibleException {
        if(!(planeBeingUsed instanceof BdaMissionVehicle))
            throw new AerialVehicleNotCompatibleException("bad plane type");
        BdaMissionVehicle MyVehicle = (BdaMissionVehicle) planeBeingUsed;
        System.out.println(pilotName+": "+((BdaMissionVehicle) planeBeingUsed).getMyPlane().getClass().getSimpleName()+" taking pictures of suspect " + this.Objective + " with : "+MyVehicle.getCameraType()+" Camera");
    }
}
