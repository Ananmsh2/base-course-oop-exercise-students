package Missions;

import AerialVehicles.AttackMissionVehicle;
import AerialVehicles.InformationMissionVehicle;
import Entities.Coordinates;

public class IntelligenceMission extends Mission {
    String region=" ";

    public IntelligenceMission(String region, Coordinates targetCords, String pilotname, InformationMissionVehicle planetouse) {
        this.region = region;
        super.pilotName=pilotname;
        super.planeBeingUsed=planetouse;
        super.TargetCords =targetCords;
    }
    @Override
    public void executeMission() throws AerialVehicleNotCompatibleException {
        if(!(planeBeingUsed instanceof InformationMissionVehicle))
            throw new AerialVehicleNotCompatibleException("bad plane type");
        InformationMissionVehicle MyVehicle=(InformationMissionVehicle)planeBeingUsed;
        System.out.println(pilotName+": "+((InformationMissionVehicle) planeBeingUsed).getMyPlane().getClass().getSimpleName()+" collecting data in "+this.region+ " with: "+MyVehicle.getSensorType() +" Sensor");
    }
}
