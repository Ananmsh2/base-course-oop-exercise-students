package Missions;

import AerialVehicles.AttackMissionVehicle;
import Entities.Coordinates;

public class AttackMission extends Mission{
    String target=" ";

    public AttackMission(String target, Coordinates targetCords, String pilotname, AttackMissionVehicle planetouse) {
        this.target = target;
        super.pilotName=pilotname;
        super.planeBeingUsed=planetouse;
        super.TargetCords =targetCords;
    }


    @Override
    public void executeMission() throws AerialVehicleNotCompatibleException {
        if(!(planeBeingUsed instanceof AttackMissionVehicle))
            throw new AerialVehicleNotCompatibleException("bad plane type");
        AttackMissionVehicle MyVehicle=(AttackMissionVehicle)planeBeingUsed;
        System.out.println(pilotName+": "+((AttackMissionVehicle) planeBeingUsed).getMyPlane().getClass().getSimpleName()+" Attacking suspect "+this.target+ " with: "+MyVehicle.getMissleType()+"X"+MyVehicle.getAmountOfMissles()+" Missles");
    }
}
