package Missions;

public interface MissionAction {

    public void executeMission() throws AerialVehicleNotCompatibleException;
}
