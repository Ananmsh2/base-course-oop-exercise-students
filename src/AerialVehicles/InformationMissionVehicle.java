package AerialVehicles;

import Entities.EnumsCreator.Sensor;

public class InformationMissionVehicle extends AerialVehicle{
    private Sensor SensorType;
    private AerialVehicle MyPlane;

    public InformationMissionVehicle(Sensor SensorType,AerialVehicle myPlane) {
        this.SensorType=SensorType;
        this.MyPlane=myPlane;
        super.baseCords=myPlane.baseCords;
        super.hoursPassedSinceCheck=myPlane.hoursPassedSinceCheck;
        super.Status=myPlane.Status;
    }

    public Sensor getSensorType() {
        return SensorType;
    }

    public AerialVehicle getMyPlane() {
        return MyPlane;
    }
}
