package AerialVehicles;
import Entities.EnumsCreator.Missle;

public class AttackMissionVehicle extends AerialVehicle{
    private int amountOfMissles;
    private Missle missleType;

    private AerialVehicle MyPlane;

    public AttackMissionVehicle(int amountOfMissles,Missle missleType,AerialVehicle myPlane) {
        this.amountOfMissles = amountOfMissles;
        this.missleType=missleType;
        this.MyPlane=myPlane;
        super.baseCords=myPlane.baseCords;
        super.hoursPassedSinceCheck=myPlane.hoursPassedSinceCheck;
        super.Status=myPlane.Status;
    }

    public int getAmountOfMissles() {
        return amountOfMissles;
    }
    public Missle getMissleType() {
        return missleType;
    }
    public AerialVehicle getMyPlane() {
        return MyPlane;
    }
}
