package AerialVehicles;

import Entities.EnumsCreator.Camera;

public class BdaMissionVehicle extends AerialVehicle{

    private Camera cameraType;
    private AerialVehicle MyPlane;

    public BdaMissionVehicle(Camera cameraType,AerialVehicle myPlane) {
        this.cameraType=cameraType;
        this.MyPlane=myPlane;
        super.baseCords=myPlane.baseCords;
        super.hoursPassedSinceCheck=myPlane.hoursPassedSinceCheck;
        super.Status=myPlane.Status;
    }
    public Camera getCameraType() {
        return cameraType;
    }

    public AerialVehicle getMyPlane() {
        return MyPlane;
    }
}
