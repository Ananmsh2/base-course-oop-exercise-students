package AerialVehicles;

import Entities.Coordinates;

public abstract class AirInformationVehicle extends AerialVehicle {


    public void HoverOverLocation(Coordinates destination)
    {
        Status = "flying";
        System.out.println("Hovering over:" + destination.getLongitude() + "," + destination.getLatitude());
    }

}
