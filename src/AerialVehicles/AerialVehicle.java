package AerialVehicles;


import Entities.Coordinates;


public abstract class AerialVehicle {

    int hoursPassedSinceCheck; //amount of hours passed since the last check
    String Status; //flight status
    public Coordinates baseCords; //coordinates of mother base
    public int  maximumHoursUntilRepair;

    public void flyTo(Coordinates destination) {
        if (this.Status == "ready"){
            System.out.println("Flying to:" + destination.getLatitude() + "," + destination.getLongitude() );
            Status = "flying";
        }
        else if (this.Status == "not ready")
        {
            System.out.println("Aerial Vehicle isn't ready to fly");
        }
    }

    public void land(Coordinates destination) {
        System.out.println("Landing on:" + destination.getLatitude() + "," + destination.getLongitude() );
        check();
    }

    public void check() {
        if(this.hoursPassedSinceCheck < this.maximumHoursUntilRepair)
            this.Status="ready";
        else
        {
            this.Status="not ready";
            repair();
        }
    }

    public void repair(){
        this.hoursPassedSinceCheck=0;
        this.Status = "ready";
    }

}
